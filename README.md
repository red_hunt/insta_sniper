# INSTA SNIPER
## Purpose
This project aim at simplifying the process of data-gathering towards somebody.
I'm implementing a telegram bot too cause I'm lazy and i don't like to use weird website/app for viewing bigger profile pic/download stories and stuff like that.
EXP++   

This repo is part of a bigger project OS-INT focused. The info class can be used stand alone passing in all methods just the profile name.

## File Content

* classes/:\
This folder contains all my classes. They're composed as follow:
    * commandhandlers.py:\
    Contains all the bot methods.
    * info.py:\
    This file contains all the "working on Instagram" methods for collecting data.


* conf/:\
This folder contains all the configuration files, not uploaded for security reasons. They're composed as follow:
    * creator.conf:\
    Save the creator chat id (add /getposts n).
    * mytarget.conf:\
    For debugging purpose this file contain a default username and set it as target for the creator chat id.
    * token_instasniper.conf:\
    Insta_sniper_bot Bot Token.
    * token_testing.conf:\
    Testing Bot Token.

* docker/:\
This folder contains all the docker file. They're composed as follow:
    * create_image.sh:\
    Build an image with all the requirements for the bot, file included. A new directory called logs will be created and shared with the docker container, all the bot logs will be stored there.
    * Dockerfile:\
    Docker configuration file for the image building process.
    * run_docker:\
    Run the instance - if already present - of instasniper bot in background, once the container stop it'll be automatically removed.
    * start_docker.sh:\
    Call create_image.sh and run_docker.sh sequentially. Create the image and run it.

* logs:\
This dir contains the logs file created when the bot is not running inside a container, used only for debug purpose.

* instasniper.py:\
Bot skeleton, this starts the bot.

* dodocker.py:\
Build the docker images and run them in a container in detached mode.

-----

## DONE
#### BOT FUNCTIONS
* profile picture
* target list: every chat have one target, avoiding to ask every time
* clear chat
* decent info banner
* "bot is sending picture >>>" status
* get bio
* follower/ing number
* private/public profile
* number of post
* active stories
* get last x post
* set target in 2 messages
* status (in a group chat and only with the creator)
* download active stories #instagram will disable the account quiet randomly so it might not work sorry
* cross-checking between followers #instagram will disable the account quiet randomly so it might not work sorry
#### CODE
* docker image and run container
* logs
-----
## Working on

-----
### TO ADD
Suggestions are welcome.

#### BOT FUNCTIONS
-----


##### NOTE
* https://instaloader.github.io/module/structures.html?highlight=bio#profiles
* https://docs.aiogram.dev/en/latest/examples/index.html
