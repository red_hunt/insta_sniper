#!/bin/bash
#create instance
rm -rf ./app ./logs ./posts
mkdir ./app ./app/conf ./app/classes ./logs ./app/posts
./create_image.sh

#create and start container
./run_docker.sh
