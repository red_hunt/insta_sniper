#!/bin/bash
#create docker image
rm -rf ./app ./logs ./posts
mkdir ./app ./app/conf ./app/classes ./logs ./app/posts ./app/posts/stories
echo "USERNAME|CHAT ID|DATA|COMMAND|TARGET|STATUS" > ./logs/log.log
cp -r ../conf/* ./app/conf/
cp -r ../classes/* ./app/classes/
cp ../instasniper.py ./app/
docker image rm redhunt/instasniper:latest
docker build --force-rm -f Dockerfile -t redhunt/instasniper .
