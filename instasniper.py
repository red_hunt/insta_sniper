#!/usr/bin/python3
import logging
import os
import datetime
from datetime import date
from classes import *
from aiogram import Bot, Dispatcher, executor,types

#check if the app is runnning in a container and set the log path
logspath='./logs/'
logsname=str(datetime.datetime.now()).split('.')[0][:-3].replace(' ','?')
priv=False
testing=False
#testing=True
if(testing == False):
    f = open("conf/token_instasniper.conf", 'r')#read the token
else:
    f = open("conf/token_testing.conf", 'r')#read the token

API_TOKEN = f.readline().rstrip()#bot token
bot = Bot(token=API_TOKEN)#bot element
dp = Dispatcher(bot)#thing that do things with the bot
allowed_chat_id=[]#who can use the bot
targets={}#who is looking for who
status={}#status for set command

last_use='Nobody:('
tmp=[]
with open("conf/creator.conf") as file_in:
    for line in file_in:
        tmp.append(line.rstrip())
creator=tmp[0]

active_since = str(datetime.datetime.now()).split('.')[0][:-3] + " UTC"

logging.basicConfig(level=logging.INFO) #debug info

command_handlers = {#command list
    "/start": welcome,
    "/info": info_handler,
    "/help": helper,
    "/who": who_following,
    '/getuserinfo': get_info,
    '/getprofilepic': profile_pic_only,
    '/clear':clear_chat,
    '/getposts':get_x_pics,
    '/getstories': get_stories,
    '/crisscrossfollowers': crisscrossfollowers,
    '/crisscrossfollowees': crisscrossfollowees
}

def write(log):
    with open(logspath+logsname+'.log','a+') as f:
        f.write(log+'\n')

def init():#read the allowed chat and set my target for debug and lazynes purpose
    global allowed_chat_id,targets,logspath,priv
    import os

    if(os.path.abspath(os.getcwd()) == '/app'):
        logspath='/app/logs/' #container path
    write("USERNAME|CHAT ID|DATA|COMMAND|TARGET|STATUS")
    mytarget()

def mytarget():#to be removed, just laziness of setting the target automatically for debug purpose
    global logspath
    lines=[]
    with open('conf/mytarget.conf') as file_in:
        for line in file_in:
            lines.append(line.rstrip())
    targets[int(lines[0].split(';')[0])] =lines[0].split(';')[1]

@dp.message_handler(commands=['add'])
async def add_allowed(message: types.Message):#set the target for a certain chat id
    global allowed_chat_id
    if(message.chat.id > 0):
        tmp =message.text[5:]
        if(message.chat.id == 216314432):
            allowed_chat_id.append(tmp)
        else:
            await message.reply("Nope")
        write(log)

@dp.message_handler(commands=['set'])
async def set_target(message: types.Message):#set the target for a certain chat id
    global last_use
    if(message.chat.id > 0):
        last_use='[ {} ]\n{} UTC'.format(message.from_user.username,str(message.date)[:-3])
        global targets,status
        status[message.chat.id]='set'
        await message.reply("Send the username")

#status
@dp.message_handler(commands=['status'])
async def state(message: types.Message):#print the bot info
    if(str(message.from_user.id) == creator):
        await bot.send_chat_action(message.chat.id,'typing')
        msg= "[ INSTASNIPER ]  🟩\n\nLast Used\n{}\n\nActive Since\n{}\n\nUp for\n{} days".format(last_use,active_since,days_up(message.date))
        await message.reply(msg)

def days_up(now):
    date1=str(active_since).split(' ')[0].split('-')
    date2=str(now).split(' ')[0].split('-')
    d1=date(int(date1[0]),int(date1[1]),int(date1[2]))
    d2=date(int(date2[0]),int(date2[1]),int(date2[2]))
    delta = d2 - d1
    return delta.days

@dp.message_handler()
async def echo(message: types.Message):#manage the income message and check if allowed and valid target
    global f2,targets,status,last_use,active_since
    targ=''
    if(message.chat.id > 0):
        #print("{} | {}".format(str(message.date),message.from_user.id))
        last_use='[ {} ]\n{} UTC'.format(message.from_user.username,str(message.date)[:-3])
        free_methods=['info','help','clear','start','crisscross']
        if(priv==False or (str(message.chat.id) in allowed_chat_id and priv==True)):
            if message.text.split(' ')[0] in command_handlers:
                if message.chat.id in targets or message.text[1:] in free_methods:
                    if(message.chat.id not in targets):
                        targ=''
                    else:
                        targ=targets[message.chat.id]
                    log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,message.text[1:],targ,'SUCCESS')
                    await command_handlers[message.text.split(' ')[0]](targ,message,bot)#command and target are valid do shit
                else:
                    log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,message.text[1:],'NONE','NO TARGET')
                    await message.reply("Set a valid username sending it at /set <username>")
            else:#not in the command list
                try:
                    if(status[message.chat.id] == 'set'):#if this id is waiting for the target save it, if not send error
                        usr=message.text
                        if('https' in usr):
                            insta='https://instagram.com/'
                            usr=usr[len(insta):].split('?')[0]

                        targets[message.chat.id] = usr.lower()

                        status[message.chat.id] = 'ready'
                        await message.reply("The current profile is set to: {}".format(targets[message.chat.id]))
                        log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,'set',targets[message.chat.id],'SUCCESS')
                    else:#error, command not valid and not waiting
                        log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,message.text[1:],targets[message.chat.id],'NOT VALID')
                        await message.reply("Invalid command!\n Try /help")
                except Exception as e:
                    log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,message.text[1:],"NONE",'NOT VALID')
                    await message.reply("Use a valid command - ask /help - or the set a target using the /set command.")

        else:
            log='{}|{}|{}|{}|{}|{}'.format(message.from_user.username,str(message.chat.id),message.date,message.text[1:],'NONE','NOT AUTH')
            await message.reply("Not Authorized!")
        write(log)


if __name__ == '__main__':
    init()
    executor.start_polling(dp, skip_updates=True)
