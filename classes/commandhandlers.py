from aiogram import types
from .info import *
import time

#start
async def welcome(name,message: types.Message,bot=''):#print the bot info
    await message.reply("Welcome!\nSet your target with /set and enjoy \n\n-Kindly suggested- For help type /help\nFor additional bot info type /info")

#info bot
async def info_handler(name,message: types.Message,bot=''):#print the bot info
    keyboard_markup = types.InlineKeyboardMarkup(row_width=2)
    keyboard_markup.add(
        # url buttons have no callback data
        types.InlineKeyboardButton('🦊 Insta Sniper 🦊', url='https://gitlab.com/red_hunt/insta_sniper'),
        types.InlineKeyboardButton('🦊 RED HUNT 🦊',url='https://gitlab.com/red_hunt/'),
        types.InlineKeyboardButton('👽 Maintainers 👽',url='https://t.me/text_grizzly_bot'),
        types.InlineKeyboardButton('🦊 Maintainers 🦊',url='https://gitlab.com/ThomasAndreatta')
    )

    await message.reply("This project is part of an OS-INT focused project that aim at simplifying the process of data-gathering towards somebody.", reply_markup=keyboard_markup)

async def helper(name,message: types.Message,bot=''):#print the bot info
    answ="Here's your help! Prepend the / in front off all your 🔧commands!"
    answ+='\n=========='
    answ+='\n\n🖖🏼start\n\nStart the bot'
    answ+='\n=========='
    answ+='\n\n❓info\n\nThe bot will print the Bot banner'
    answ+='\n=========='
    answ+='\n\n🙏🏼help\n\nThe bot will print the help menu'
    answ+='\n=========='
    answ+='\n\n⚙️set\n\nAfter this command send the username/link of your target'
    answ+='\n=========='
    answ+='\n\n🤌🏼who\n\nThe bot will print the current target'
    answ+='\n=========='
    answ+='\n\n🕵🏼‍♂️userinfo\n\nThe bot will print all the instagram-info of your target'
    answ+='\n=========='
    answ+='\n\n🖼getprofilepic\n\nThe bot will send the profile picture of the target'
    answ+='\n=========='
    answ+='\n\n📂getposts\n\nThe bot will send the last 3 posts of the target, for more posts use \ngetposts x\nwhere x is the number of posts'
    answ+='\n=========='
    answ+='\n\n🤳🏻getstories\n\nThe bot will send all the active stories of the target'
    answ+='\n=========='
    answ+='\n\n👨🏼‍💻crisscrossfollowers\n\nThe bot will send all the users that follow your target list, send the list(5 targets max) as follow(using spaces):\ncrisscrossfollowers <target1> <target2>'
    answ+='\n=========='
    answ+='\n\n👨🏼‍💻crisscrossfollowees\n\nThe bot will send all the users that are followed by your target list, send the list(5 targets max) as follow(using spaces):\ncrisscrossfollowees <target1> <target2>'
    answ+='\n=========='
    answ+='\n\nPs: if you keep getting "user not found" or "Not sufficent sauce for crisscross" could be due to many GLOBAL requests, try later, i\'m sorry instagram sucks '

    await message.reply(answ)


#show target
async def who_following(name,message: types.Message,bot=''): #send back who you're following
    await message.reply("The current profile is {}".format(name))

#get target info
async def get_info(name,message: types.Message,bot=''):#print the bot info
    await bot.send_chat_action(message.chat.id,'typing')
    await message.reply(info.get_info(name))

#get target profile pic
async def profile_pic_only(name,message: types.Message,bot=''):#send back the profile picture or an error message
    import shutil
    await bot.send_chat_action(message.chat.id,'upload_photo')
    pic=info.get_profile_pic(name)
    if(pic != '-1'):
        with open(pic,'rb') as photo:
            await message.reply_photo(photo, caption="Profile picture of {}".format(name))
            shutil.rmtree(pic.split('/')[1])
    else:
        await message.reply("Username [ {} ] not found!".format(name))

#get last x profile pic
async def get_x_pics(name,message: types.Message,bot=''):#send back the profile picture or an error message
    import shutil

    await message.answer("Could take a while")# WARNING: \n\n[hello](https://t.me/)",parse_mode ='markdown'
    await bot.send_chat_action(message.chat.id,'upload_photo')
    name=name+ ' '+str(message.chat.id)#create the path name
    try:
        shutil.rmtree('./posts/{}'.format(name))
    except Exception as e:
        pass


    instagramurl='https://instagram.com/'+name.split(' ')[0]+'?utm_medium=copy_link'
    if(len(message.text.split(' '))> 1): #download x picture
        pic=info.get_pics(name,max=int(message.text.split(' ')[1]))
    else:
        pic=info.get_pics(name)

    if isinstance(pic, str):
        await message.reply(pic)
    else:

        for set in pic:
            media = types.MediaGroup()

            post_data=set[1].split('/')[len(set[1].split('/'))-1].split('_')[0] #extract the date in a fancy way lol
            #set[0] is the post description
            description='Username: \n[@{}]({})\nDate: {}\n{}'.format(name.split(' ')[0],instagramurl,post_data,info.parse_md(set[0]))

            media.attach_photo(types.InputFile(set[1]),description,parse_mode ='markdown')#attach the first photo with the description

            for index in range(2,len(set)):#attach the remaining photo

                media.attach_photo(types.InputFile(set[index]))

            await message.reply_media_group(media=media)
        shutil.rmtree('./posts/{}'.format(name))


    await bot.delete_message(message.chat.id,str(int(message.message_id)+1))

#get target active stories
async def get_stories(name,message: types.Message,bot=''):#get all the target info and return them in string format

    import shutil
    await message.answer("Could take a while")
    name=name+ ' '+str(message.chat.id)#create the path name
    #for coming back later
    cdir=os.getcwd()
    try:#remove the dir if already present
        shutil.rmtree('./posts/stories/{}'.format(name))
    except Exception as e:
        pass

    pic=info.retrive_stories(name)#retrive

    instagramurl='https://instagram.com/'+name.split(' ')[0]+'?utm_medium=copy_link'
    if(len(pic) != 0 and not isinstance(pic, str)):#if there's some pic or not string (username not found, error and stuff)
        for set in pic:
            media = types.MediaGroup()
            #set[0] is the element path
            description='Username: \n[@{}]({})\nDate: {}\ntime: {}'.format(name.split(' ')[0],instagramurl,set[1],set[2])

            if(set[0].endswith('.mp4')):#attach video or pic
                media.attach_video(types.InputFile(set[0]),caption=description,parse_mode ='markdown')#attach the first photo with the description
            else:
                media.attach_photo(types.InputFile(set[0]),caption=description,parse_mode ='markdown')#attach the first photo with the description
            await message.reply_media_group(media=media)
        try:
            os.chdir(cdir)#here it's later, remove the
            shutil.rmtree('./posts/stories/{}'.format(name))
        except Exception as e:
            print("NOPE: ",e)

    else:
        await message.reply(pic)
    await bot.delete_message(message.chat.id,str(int(message.message_id)+1))

#cross check who follows all the targets
async def crisscrossfollowers(name,message: types.Message,bot=''):#print the bot info
    txt =message.text.replace('\n',' ')
    target=txt.split(' ')[1:]
    target=[x.strip() for x in target if x.strip()]
    checked,lprivate,notvalid,sauce=info.crisscross(target,1)
    description=''

    if(len(checked) > 0):
        description+='✅ Valid ✅\n'
        for x in checked:
            description+='{}\n'.format(x)

    if(len(lprivate) > 0):
        description+='\n⛔️ Private ⛔️\n'
        for x in lprivate:
            description+='{}\n'.format(x)

    if(len(notvalid) > 0):
        description+='\n⚙️ Not valid ⚙️\n'
        for x in notvalid:
            description+='{}\n'.format(x)

    if(isinstance(sauce, str)):
        await message.reply("{}\n{}".format(description,sauce),parse_mode ='markdown')
    else:
        description += "\n💰 Your Applesauce 💰\n"
        for x in sauce:
            instagramurl='https://instagram.com/'+x+'?utm_medium=copy_link'
            description+='[@{}]({})\n'.format(x,instagramurl)

        await message.reply("{}".format(description),parse_mode ='markdown')

#cross check who is followed from all target
async def crisscrossfollowees(name,message: types.Message,bot=''):#print the bot info
    txt =message.text.replace('\n',' ')
    target=txt.split(' ')[1:]
    target=[x.strip() for x in target if x.strip()]
    checked,lprivate,notvalid,sauce=info.crisscross(target,2)
    description=''

    if(len(checked) > 0):
        description+='✅ Valid ✅\n'
        for x in checked:
            description+='{}\n'.format(x)

    if(len(lprivate) > 0):
        description+='\n⛔️ Private ⛔️\n'
        for x in lprivate:
            description+='{}\n'.format(x)

    if(len(notvalid) > 0):
        description+='\n⚙️ Not valid ⚙️\n'
        for x in notvalid:
            description+='{}\n'.format(x)

    if(isinstance(sauce, str)):
        await message.reply("{}\n{}".format(description,sauce),parse_mode ='markdown')
    else:
        description += "\n💰 Your Applesauce 💰\n"
        for x in sauce:
            instagramurl='https://instagram.com/'+x+'?utm_medium=copy_link'
            description+='[@{}]({})\n'.format(x,instagramurl)

        await message.reply("{}".format(description),parse_mode ='markdown')

#clear chat
async def clear_chat(name,message: types.Message,bot=''): #send back who you're following
    await bot.send_chat_action(message.chat.id,'typing')
    for x in range(0,message.message_id):
        try:
            await bot.delete_message(message.chat.id,str(x))
        except:
            pass
    await message.answer('Everything clear!')
    await bot.delete_message(message.chat.id,message.message_id)
