from instaloader import *
import os
import dotenv # python-dotenv

class info:
    dotenv.load_dotenv()
    def valid_name(name): #check if the username is valid, sometimes it fails, better use try catch as support
        import requests
        headers= {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36"}
        return 200 == requests.get('https://www.instagram.com/'+name,headers=headers ).status_code

    def parse_md(txt):
        import re
        match_md = r'((([_*]).+?\3[^_*]*)*)([_*])'
        return re.sub(match_md, "\g<1>\\\\\g<4>", txt)

    def extractinfo(post):
        tmp=''
        desc='Like: {}\n'.format(post.likes)

        if(len(post.tagged_users)>0):
            for x in post.tagged_users:
                instagramurl='https://instagram.com/'+x+'?utm_medium=copy_link'
                tmp+='[@{}]({})\n'.format(x,instagramurl)
            desc+='\nTagged:\n'+tmp
        else:
            desc+= '\nTagged: Nobody\n'
        desc+='\nCaption: \n=====\n{}'.format(post.caption)
        return desc


    def get_pics(name,max=3):#get all the target info and return them in string format
        import os
        username = name.split(' ')[0]
        #print('getpics : {}/posts/{}'.format(os.getcwd(),name))
        try: #make sure that the folder does not exists
            #print("delete")
            shutil.rmtree('./posts/{}'.format(name))
        except:
            pass
        c=0 #counter
        try:
            if(info.valid_name(username)):
                mod = instaloader.Instaloader(download_videos=False,save_metadata=False,download_video_thumbnails=False,quiet=True,download_geotags=True)
                profile = Profile.from_username(mod.context,username)
                private=profile.is_private

                path_list = [[''] for x in range(0,max)]

                if(private == False):
                    posts = profile.get_posts()


                    os.mkdir('./posts/{}'.format(name))
                    os.chdir("./posts/{}".format(name))#create a default tmp directory
                    for post in posts:
                        if(post.typename != 'GraphVideo'):
                            tmp_path=os.getcwd()+'/'+str(c)
                            if(c<max):
                                mod.download_post(post, "{}".format(str(c)))#download into the dir "posts/username/iterator"
                                for file in os.listdir(tmp_path):
                                    if file.endswith(".jpg"):
                                        path_list[c].append(tmp_path+'/'+file)#if pic save the path
                                path_list[c][0]=info.extractinfo(post)
                                c=c+1
                            else:
                                break

                    os.chdir('../..')
                    return path_list
                else:
                    return "Profile is private"
            else:
                return 'Username [ {} ] not found!'
        except Exception as e:
            print("SOMETHING WENT WRONG:",e)
            return "An error occurred!"


    def get_profile_pic(name): #downlaod profile pic and send back the path or -1 if fail
        import os
        path='-1'
        mod=instaloader.Instaloader(quiet=True)
        try:
            if(info.valid_name(name)):
                mod.download_profile(name,profile_pic_only=True)
                for file in os.listdir("{}".format(name.lower())):
                    if file.endswith(".jpg"):
                        path="./{}/".format(name).lower()+file
                return path
            else:
                return '-1'
        except:
            return '-1'

    def get_info(name):#get all the target info and return them in string format
        val="Username [ {} ] not found!".format(name)
        if(info.valid_name(name)):
            try:
                mod=instaloader.Instaloader()
                profile=Profile.from_username(mod.context,name)

                username=profile.username
                full_name=profile.full_name
                bio=profile.biography
                priv=profile.is_private
                followers=profile.followers
                followees=profile.followees
                npost=profile.mediacount
                activestories=profile.has_viewable_story
                val = "Username: {}\nN posts: {}\nFull name: {}\nIs private: {}\nBio: \n===== \n{}\n=====\nFollowers: {}\nFollows: {}\nActive stories: {}".format(username,str(npost),full_name,priv,bio,followers,followees,activestories)
                return val
            except Exception as e:

                return val
        else:
            return val

    def get_followers(name):
        val="Username [ {} ] not found!".format(name)
        from instaloader import Instaloader, Profile
        loader = Instaloader()
        if(info.valid_name(name)):
            try:
                loader.load_session_from_file('instasniper_redhunt', filename="../conf/sessionfile")

                profile = Profile.from_username(loader.context, name)
                followers = profile.get_followers()
                val = followers
                return val
            except Exception as e:
                print(e)
                return val
        else:
            return val

    def crisscross(target):
        loader = Instaloader()
        loader.load_session_from_file('instasniper_redhunt', filename="../conf/sessionfile")
        #get all the followers
        sauce=[]
        lista = []
        checked=[]
        if(len(target)<2):
            return "Not sufficent target for sauce"

        for x in target:
            try:
                profile = Profile.from_username(loader.context, x)
                private=profile.is_private

                if(private == False):
                    followers = profile.get_followers()
                    tmp=[]
                    for y in followers:
                        tmp.append(y.username)
                    lista.append(tmp)
                    checked.append('[ ✳️Valid✳️ ] {}'.format(x))
                else:
                    checked.append('[ ❌Private❌ ] {}'.format(x))
            except Exception as e:
                print("Bop, element not valid in criss cross")
                return "{} is not valid/is private".format(x)

        if(len(lista)<2):
            return [checked, "Not sufficent target for sauce, probably some target is not valid"]

        #choose the shortest list to iteret trough
        pos=0
        min=len(lista[pos])

        for x in range(0,len(lista)):
            if(min> len(lista[x])):
                pos=x
                min=len(lista[x])

        #iterate trough the shortest list of followers        
        for el in lista[pos]:
            present = True
            #if present in all add to the sauce
            for y in range(0,len(lista)):
                #jump the check on itself
                if(y!=pos):
                    if el not in lista[y]:
                        present=False
                        break
                else:
                    break
            if(present):
                sauce.append(el)

        return [checked,sauce]
